package main

import (
	"C"
	"fmt"
	"os"
	"time"
)

//COLORES
var ResetarColor string = "\033[0m"
var VerdeIntenso string = "\033[0;92m"
var AmarilloIntenso string = "\033[0;93m"
var AzulIntenso string = "\033[0;96m"
var RojoIntenso string = "\033[0;91m"

//OPCIÓN A ELEGIR
var opcion int

//STRUCT BARRA
type varibale struct {
	N int
	DELAY time.Duration
	veces int
	columnas int
}

func main() {

	menu()

	// SELECCIONA LA OPCIÓN
	fmt.Println(ResetarColor + "Selecciona una opción: ");
	fmt.Scanf("%d", &opcion);

	conectando()

	switch opcion {
		case 1:
			fmt.Println(AzulIntenso + "Para apagar la TV, tiene que presiona Ctrl+X, luego pulse Esc/Q y por último presionar Enter")
		default:
			fmt.Println(AzulIntenso + "La opcion seleccionada no está en el menú")
	}
}

func menu(){
	// MENU
	fmt.Println(VerdeIntenso + "**************************************************")
	fmt.Println("*                                                *")
	fmt.Println("*          "+ AmarilloIntenso + "LISTA DE OPCIONES PARA LA TV "+ VerdeIntenso + "         *")
	fmt.Println("*          "+ ResetarColor + "1. APAGAR LA TV              "+ VerdeIntenso + "         *")
	fmt.Println("*                                                *")
	fmt.Println("**************************************************")
}
//BARRA CONECTADO A LA TELEVISIÓN
func conectando() {

	v := varibale{
		N: 100,
		DELAY: 100,
		veces: 0,
		columnas: 0,

	}

	for v.veces<v.N {
		for v.columnas < v.veces {
			fmt.Fprintf(os.Stderr, RojoIntenso+"=")
			v.columnas++;
		}
		fmt.Fprintf(os.Stderr, RojoIntenso+"Conectando a la televisón. Por favor, espere. %d%%\r", v.veces)
		time.Sleep(v.DELAY * time.Millisecond)
		v.veces++;
	}
	fmt.Printf(ResetarColor)

}
