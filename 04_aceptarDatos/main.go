package main

import (
	"bufio"
	"fmt"
	"os"
)

var numero1 int
var numero2 int
var resultado int
var leyenda string

func main() {
	fmt.Println("Ingrese el primer número: ")
	fmt.Scanf("%d", &numero1)
	fmt.Println("Ingrese el segundo número: ")
	fmt.Scanf("%d", &numero2)

	fmt.Println("Introduce una descripción: ")
	scanner := bufio.NewScanner(os.Stdin)

	if scanner.Scan() {
		leyenda = scanner.Text()
	}
	resultado = numero1 + numero2
	fmt.Printf("La %s de los números %d y %d es %d", leyenda, numero1, numero2, resultado)
	
	
}
