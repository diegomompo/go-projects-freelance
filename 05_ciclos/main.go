package main

import "fmt"

func main() {
	for i:=1; i<=10; i++{
		fmt.Println(i)
	}

	numero:=0
	for{
		fmt.Println("Continuo")
		fmt.Println("Ingrese un número")
		fmt.Scanf("%d", &numero)

		if numero == 100{
			break
		}
	}

	var i = 0
	for i<0{
		fmt.Printf("\n Valor de i: %d", i)
		if i==5{
			fmt.Printf("multiplicamos por 2 \n")
			i=i*2
			continue
		}
		fmt.Printf("  Paso por aqui")
		i++
	}

	var j int = 0

	RUTINA:
	for j < 10 {
		if j == 4{
			j=j+2
			fmt.Printf("voy a rutina sumando 2")
			goto RUTINA
		}
		fmt.Printf("Valor de j: %d\n", j);
		j++
	}
}
