package main

import "fmt"

var Calculo func(int, int) int = func(num1 int, num2 int ) int{
	return num1 + num2
}

func main() {
	fmt.Printf("Sumo 5 + 7 = %d\n", Calculo(5,7))

	//resta
	Calculo = func(num1 int, num2 int) int {
		return num1 - num2
	}
	fmt.Printf("Resto 6 - 4 = %d\n", Calculo(6,4))

	//dividir
	Calculo = func(num1 int, num2 int) int {
		return num1 / num2
	}
	fmt.Printf("Divido 12 / 3 = %d\n", Calculo(12,3))
	Operaciones()

	/*closures*/
	tabla2 := 2
	MiTabla:=Tabla(tabla2)

	for i:=1; i<20; i++{
		fmt.Println(MiTabla())
	}
}

	func Operaciones(){
		resultado:= func() int{
			var a int = 23
			var b int = 13
			return a + b
		}
		fmt.Println(resultado())
	}

	func Tabla(valor int) func() int {
		numero:=valor
		secuencia:=0
		return func() int {
			secuencia++
			return numero * secuencia
		}

	}

