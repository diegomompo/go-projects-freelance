package main

import "fmt"

var matriz [5][7]int
func main() {

	//MATRIZ
	matriz[3][5] = 1
	fmt.Println(matriz)


	// VECTOR
	tabla := [10]int{5,6,98,1,4,0,3,65,8,99}

	/*for i:=0; i<len(tabla); i++{
		fmt.Println(tabla[i])
	}*/
	fmt.Println(tabla)

	//VECTORES DINÁMICOS
	matriz2:=[]int{2,5,4}
	fmt.Println(matriz2)
	variante2()
	variante3()
	variante4()
}
func variante2(){
	elementos := [5]int{1,2,3,4,5}
	porcion:= elementos[2:4]

	fmt.Println(porcion)
}
func variante3(){
	elementos := make([]int, 5, 20)
	fmt.Printf("Largo %d, Capacidad %d\n", len(elementos), cap(elementos))


}
func variante4(){
	nums := make([]int, 0, 0)
	for i := 0; i<100; i++ {
		nums = append(nums, i)
	}
	fmt.Printf("Largo: %d, Capacidad %d\n", len(nums), cap(nums))
}