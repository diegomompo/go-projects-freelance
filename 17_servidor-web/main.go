package main

import "net/http"

func main() {
	http.HandleFunc("/", home)
	http.ListenAndServe(":3000", nil)

}

func home(w http.ResponseWriter, r *http.Request){
	http.ServeFile(w, r, "go/17_servidor-web/index.html")
}
